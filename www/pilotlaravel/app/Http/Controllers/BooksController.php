<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BooksController extends Controller
{
   /* function  store(Request $request){
        $book = new Books();
        $book->setAttribute("name",$request->name);
        $book->setAttribute("category",$request->category);
        $book->setAttribute("description".$request->descript);
        if($book->save()){
            return true;
        }
    }*/
    function  store(Request $request){
        if(Books::created($request->all())){
            return true;
        }
    }
    function update(Request $request,Books $book){
        if($book->fill($request->all())->save()){
            return true;
        }

    }
    function index(){
        $books=Books::all();
        return $books;
    }
    function shows(Books $book){
        return $book;

    }
    function destroy(Books $books){
        if($books->delete()){
            return true;
        }
    }

}
