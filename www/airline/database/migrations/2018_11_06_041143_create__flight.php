<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Flight', function (Blueprint $table) {
            $table->increments('Fid');
            $table->integer("Sid");
            $table->string("Source",100);
            $table->string("Destination",100);
            $table->Datetime("Start_time");
            $table->Datetime("End_time");
            $table->string("Plane_name",100);
            $table->integer("Price");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Flight');
    }
}
