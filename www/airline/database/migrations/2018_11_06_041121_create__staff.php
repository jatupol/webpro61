<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Staff', function (Blueprint $table) {
            $table->increments('Sid');
            $table->string("password",20);
            $table->string("first_name",100);
            $table->string("last_name",100);
            $table->string("Email",100);
            $table->string("Sex",10);
            $table->string("ID_Card",10);
            $table->string("Address",100);
            $table->string("Phone",10);
            $table->string("Position",20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Staff');
    }
}
