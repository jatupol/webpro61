<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Customer', function (Blueprint $table) {
            $table->increments('Cid');
            $table->string("first_name",20);
            $table->string("last_name",20);
            $table->string("Email",100);
            $table->string("Password",10);
            $table->string("Sex",10);
            $table->string("ID_Card",10);
            $table->string("Address",10);
            $table->integer("Phone");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Customer');
    }
}
